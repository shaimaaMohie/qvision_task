//function for fire owl carousel
$('#main-service').owlCarousel({
    items: 3,
    loop: false,
    nav: true,
    dots: false,
    autoplay: false,
    rtl: true,
    navText: [
        "<i class='fa fa-angle-right'></i>",
        "<i class='fa fa-angle-left'></i>"
    ],
    responsive: {
        320: {
            margin: 35,
            stagePadding: 35,
            items: 1
        },
        360: {
            margin: 35,
            stagePadding: 35,
            items: 1
        },
        400: {
            margin: 60,
            stagePadding: 60,
            items: 1
        },
        500: {
            margin: 95,
            stagePadding: 95,
            items: 1
        },
        600: {
            margin: 95,
            stagePadding: 95,
            items: 2
        },
        767: {
            margin: 110,
            stagePadding: 110,
            items: 2
        },
        1024: {
            margin: 120,
            stagePadding: 120,
            items: 3
        }
    }
});

$('#main-ads').owlCarousel({
    items: 1,
    loop: false,
    nav: true,
    dots: false,
    autoplay: true,
    rtl: true,
    navText: [
        "<i class='fa fa-angle-right'></i>",
        "<i class='fa fa-angle-left'></i>"
    ],
    responsive: {
        320: {
            margin: 35,
            stagePadding: 35,
        },
        360: {
            margin: 35,
            stagePadding: 35,
        },
        400: {
            margin: 60,
            stagePadding: 60,
        },
        500: {
            margin: 95,
            stagePadding: 95,
        },
        600: {
            margin: 95,
            stagePadding: 95,
        },
        767: {
            margin: 110,
            stagePadding: 110,
        },
        1024: {
            margin: 120,
            stagePadding: 120,
        }
    }
});
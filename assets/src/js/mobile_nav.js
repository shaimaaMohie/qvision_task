//function for mobile nav 
$('.nav-btn').on('click', function() {
    if ($(this).hasClass('changeIcon')) {
        $('footer').removeClass('nav_opened');
        $('.page-wrapper').removeClass('nav_opened');
        $('.nav').removeClass('active');
        $(this).toggleClass('changeIcon');

    } else {
        $('footer').addClass('nav_opened');
        $('.page-wrapper').addClass('nav_opened');
        $('.nav').addClass('active');
        $(this).addClass('changeIcon');
    }
});